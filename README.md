# Guess-a-number_Game

Simple guess a number java game.

---

##Introduction

In this game, the computer will come up with one random number from a specific range.  
The player must enter next numbers on console, until he guesses the right one.  
For every guess, the computer will either say "Too high" or "Too low", and then ask for another input.  
At the end of game, the number is revealed along with the number of guesses it took to get the correct number.  

![wedding-app-screen-tour](guess-a-number-preview.gif)

---

##Technologies

* Java

---

##Setup

To run this program, clone repository and compile files using your own IDE.

~~~
git clone https://glowkapracuje@bitbucket.org/glowkapracuje/guess-a-number_game.git
~~~