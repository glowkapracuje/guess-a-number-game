package pl.glowkapracuje;

import java.util.Random;
import java.util.Scanner;

public class GuessAName {

    private static final int RANGE = 100;

    private static final String HELLO_TEXT = "Hello! Let's start a game. Guess my number, type one from range: " + RANGE;
    private static final String ALERT_NO_NUMBER = "Type a number, not a letter";
    private static final String ALERT_OUT_OF_RANGE = "Your number is out of range, type one from: " + RANGE;
    private static final String MSSG_TOO_HIGH = "Your number is too high!";
    private static final String MSSG_TOO_LOW = "Your number is too low!";
    private static final String MSSG_WIN = "Yes! This is my number! You guessed after the attempt: ";


    public static void main(String[] args) {

        System.out.println(HELLO_TEXT);

        playTheGame(RANGE);

    }


    /*
    COMPARE COMPUTER NUMBER AND PLAYERS ONES, DISPLAY RESULT AND NUMBER OF STEPS
     */
    private static void playTheGame(int range) {

        int playerNum = scanNumber(range);
        int compNum = randomNumber(range);
        int guessCount = 0;

        while (playerNum != compNum) {

            if (playerNum > compNum) {
                System.out.println(MSSG_TOO_HIGH);
                playerNum = scanNumber(range);
                guessCount++;
            } else {
                System.out.println(MSSG_TOO_LOW);
                playerNum = scanNumber(range);
                guessCount++;
            }
        }
        guessCount++;

        if (guessCount == 1) {
            System.out.println("Mo mo mo mo monster kill...");
        }

        System.out.println(MSSG_WIN + guessCount);
    }


    /*
    THE COMPUTER DRAWS A NUMBER FROM THE GIVEN RANGE
     */
    private static int randomNumber(int range) {
        Random random = new Random();
        return random.nextInt(range) + 1;
    }


    /*
    SCANNING PLAYER NUMBER FROM CONSOLE
     */
    private static int scanNumber(int range) {
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNext()) {
            if (scanner.hasNextInt()) {
                // is a number? let's check the range
                int scannedNum = scanner.nextInt();
                if (scannedNum > 0 && scannedNum <= range) {
                    return scannedNum;
                } else {
                    System.out.println(ALERT_OUT_OF_RANGE);
                }
            } else {
                // is not a number? display alert and stay in loop
                System.out.println(ALERT_NO_NUMBER);
                scanner.next();
            }
        }

        return scanner.nextInt();
    }

}